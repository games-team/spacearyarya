#ifndef _MY_H_
#define _MY_H_

RcHitEnum MoveShot(CharacterData *my);
RcHitEnum MoveMy(CharacterData *my);
void CreateMy(void);
RcHitEnum HitMy(CharacterData *my, CharacterData *your);
RcHitEnum MoveDie(CharacterData *my);

#endif
