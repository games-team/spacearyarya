#ifndef _RANKING_H_
#define _RANKING_H_

void RankingScore(void);
void ReadScore(void);
void ScoreRanking(void);
void WriteScore(void);

#endif
