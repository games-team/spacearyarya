#ifndef _YOUR_H_
#define _YOUR_H_

void CreateEnemy1(Sint16 x, Sint16 y);
void CreateEnemy2(Sint16 x, Sint16 y);
void CreateEnemyShot(Sint16 x, Sint16 y, Sint16 z);
void CreateWall(Sint16 x);
RcHitEnum HitEnemyToBomb(CharacterData *my, CharacterData *your);
RcHitEnum MoveBomb(CharacterData *my);
RcHitEnum MoveBound(CharacterData *my);
RcHitEnum MoveEnemyShot(CharacterData *my);
RcHitEnum MoveWall(CharacterData *my);

#endif
